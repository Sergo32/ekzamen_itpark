﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using HtmlAgilityPack;
using DbClassLibrary.Entities;
using DbClassLibrary;


namespace Parser
{

    class Program
    {

        static void GetAllCitiesUrlsForPopularCars(string initialUrl)
        {
            WebClient webClient = new WebClient();
            string htmlCode = webClient.DownloadString(initialUrl);

            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(htmlCode);
            string node = htmlDocument.DocumentNode.SelectSingleNode("//table[@class='t-1 tablesorter']").InnerHtml;
            htmlDocument.LoadHtml(node);
            HtmlNodeCollection htmlNodes = htmlDocument.DocumentNode.SelectNodes("//tr[@class='even']");
            HtmlNodeCollection htmlNodesBlack = htmlDocument.DocumentNode.SelectNodes("//tr[@style='background-color: rgb(255, 255, 255);']");
            foreach (var item in htmlNodesBlack)
            {
                htmlNodes.Add(item);
            }
            DbManager.GetInstance().tableBank.ClearBank();
            foreach (var item in htmlNodes)
            {
                Bank bank = new Bank
                {
                    Name = item.ChildNodes[0].InnerText.Replace("&nbsp; &nbsp;", ""),
                    BuyDollarPrice = double.Parse(item.ChildNodes[1].InnerText),
                    SellDollarPrice = double.Parse(item.ChildNodes[2].InnerText),
                    BuyEuroPrice = double.Parse(item.ChildNodes[3].InnerText),
                    SellEuroPrice = double.Parse(item.ChildNodes[4].InnerText),
                    DT = item.ChildNodes[5].InnerText,
                };
                DbManager.GetInstance().tableBank.InsertBank(bank);
            }
        }

        static void Main(string[] args)
        {
            string initialUrl = "file:///C:/Users/%D0%90%D0%B2%D1%82%D0%BE%D1%80/Desktop/%D0%9B%D1%83%D1%87%D1%88%D0%B8%D0%B5%20%D0%BA%D1%83%D1%80%D1%81%D1%8B%20%D0%B2%D0%B0%D0%BB%D1%8E%D1%82%20%D0%B2%20%D0%B1%D0%B0%D0%BD%D0%BA%D0%B0%D1%85%20%D0%91%D1%80%D1%8F%D0%BD%D1%81%D0%BAa%20-%20%D0%B2%D1%8B%D0%B3%D0%BE%D0%B4%D0%BD%D1%8B%D0%B9%20%D0%BE%D0%B1%D0%BC%D0%B5%D0%BD%20%D0%B4%D0%BE%D0%BB%D0%BB%D0%B0%D1%80%D0%B0%20%D0%B8%20%D0%B5%D0%B2%D1%80%D0%BE.html";

            GetAllCitiesUrlsForPopularCars(initialUrl);

        }
    }

}

