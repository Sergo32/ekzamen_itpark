﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using Newtonsoft.Json;
using DbClassLibrary.Entities;
using DbClassLibrary;

namespace WFA_Banks_Client
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private async void LoadBanksName_Click(object sender, EventArgs e)
        {
            string url = "http://localhost:58058/ServiceBank.svc/rest/GetAllBankNames";
            WebClient webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;

            string json = await Task<string>.Factory.StartNew(()=> webClient.DownloadString(url));
            List<Bank> names = JsonConvert.DeserializeObject<List<Bank>>(json);

            comboBoxBanksName.DataSource = null;
            comboBoxBanksName.DataSource = names;
        }

        private async void buttonLoadBank_Click(object sender, EventArgs e)
        {
            string name = comboBoxBanksName.SelectedItem.ToString();

            string url = $"http://localhost:58058/ServiceBank.svc/rest/GetBankByName?name={name}";
            WebClient webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;

            string json = await Task<string>.Factory.StartNew(() => webClient.DownloadString(url));
            List<Bank> bank = JsonConvert.DeserializeObject<List<Bank>>(json);

            dataGridViewBanks.DataSource = null;
            dataGridViewBanks.DataSource = bank;
        }

        private async void buttonLoadAll_Click(object sender, EventArgs e)
        {
            
            string url = $"http://localhost:58058/ServiceBank.svc/rest/GetAllTable";
            WebClient webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;

            string json = await Task<string>.Factory.StartNew(() => webClient.DownloadString(url));
            List<Bank> bank = JsonConvert.DeserializeObject<List<Bank>>(json);

            dataGridViewBanks.DataSource = null;
            dataGridViewBanks.DataSource = bank;
        }

        private async void buttonBuyDollar_Click(object sender, EventArgs e)
        {
            string url = $"http://localhost:58058/ServiceBank.svc/rest/GetDollarToBuy";
            WebClient webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;

            string json = await Task<string>.Factory.StartNew(() => webClient.DownloadString(url));
            List<Bank> bank = JsonConvert.DeserializeObject<List<Bank>>(json);

            dataGridViewBanks.DataSource = null;
            dataGridViewBanks.DataSource = bank;
        }

        private async void buttonSellDollar_Click(object sender, EventArgs e)
        {
            string url = $"http://localhost:58058/ServiceBank.svc/rest/GetDollarToSell";
            WebClient webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;

            string json = await Task<string>.Factory.StartNew(() => webClient.DownloadString(url));
            List<Bank> bank = JsonConvert.DeserializeObject<List<Bank>>(json);

            dataGridViewBanks.DataSource = null;
            dataGridViewBanks.DataSource = bank;
        }

        private async void buttonBuyEuro_Click(object sender, EventArgs e)
        {
            string url = $"http://localhost:58058/ServiceBank.svc/rest/GetEuroToBuy";
            WebClient webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;

            string json = await Task<string>.Factory.StartNew(() => webClient.DownloadString(url));
            List<Bank> bank = JsonConvert.DeserializeObject<List<Bank>>(json);

            dataGridViewBanks.DataSource = null;
            dataGridViewBanks.DataSource = bank;
        }

        private async void buttonSellEuro_Click(object sender, EventArgs e)
        {
            string url = $"http://localhost:58058/ServiceBank.svc/rest/GetEuroToSell";
            WebClient webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;

            string json = await Task<string>.Factory.StartNew(() => webClient.DownloadString(url));
            List<Bank> bank = JsonConvert.DeserializeObject<List<Bank>>(json);

            dataGridViewBanks.DataSource = null;
            dataGridViewBanks.DataSource = bank;
        }
    }
}
