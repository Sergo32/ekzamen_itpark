﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using DbClassLibrary;

namespace WFA_Banks_Client
{
    public partial class WFA_Auth : Form
    {
        public WFA_Auth()
        {
            InitializeComponent();
        }

        private void buttonEnter_Click(object sender, EventArgs e)
        {
            Wrapper wrapper = new Wrapper();
            wrapper.Login = textBoxLogin.Text;
            wrapper.Password = textBoxPassword.Text;

            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            string uri = "http://localhost:58058/ServiceBank.svc/rest/Auth";

            client.Headers[HttpRequestHeader.ContentType] = "application/json";

            string json = JsonConvert.SerializeObject(wrapper);
            string responseJson = client.UploadString(uri, "POST", json);

            bool result = JsonConvert.DeserializeObject<bool>(responseJson);
            if (!result)
            {
                MessageBox.Show("Неверный логин или пароль.");
                return;
            }
            else
            {
                MessageBox.Show("Ок. Сейчас программа будет запущена.");
                this.Hide();
                new FormMain().ShowDialog();
                this.Close();
            }
        }
    }
}
