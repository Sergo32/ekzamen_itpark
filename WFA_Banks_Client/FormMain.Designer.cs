﻿namespace WFA_Banks_Client
{
    partial class FormMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.LoadBanksName = new System.Windows.Forms.Button();
            this.buttonLoadBank = new System.Windows.Forms.Button();
            this.buttonLoadAll = new System.Windows.Forms.Button();
            this.dataGridViewBanks = new System.Windows.Forms.DataGridView();
            this.comboBoxBanksName = new System.Windows.Forms.ComboBox();
            this.buttonBuyDollar = new System.Windows.Forms.Button();
            this.buttonSellDollar = new System.Windows.Forms.Button();
            this.buttonBuyEuro = new System.Windows.Forms.Button();
            this.buttonSellEuro = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBanks)).BeginInit();
            this.SuspendLayout();
            // 
            // LoadBanksName
            // 
            this.LoadBanksName.Location = new System.Drawing.Point(224, 12);
            this.LoadBanksName.Name = "LoadBanksName";
            this.LoadBanksName.Size = new System.Drawing.Size(322, 23);
            this.LoadBanksName.TabIndex = 0;
            this.LoadBanksName.Text = "Загрузить список банков";
            this.LoadBanksName.UseVisualStyleBackColor = true;
            this.LoadBanksName.Click += new System.EventHandler(this.LoadBanksName_Click);
            // 
            // buttonLoadBank
            // 
            this.buttonLoadBank.Location = new System.Drawing.Point(12, 284);
            this.buttonLoadBank.Name = "buttonLoadBank";
            this.buttonLoadBank.Size = new System.Drawing.Size(259, 23);
            this.buttonLoadBank.TabIndex = 1;
            this.buttonLoadBank.Text = "Загрузить выбранный банк";
            this.buttonLoadBank.UseVisualStyleBackColor = true;
            this.buttonLoadBank.Click += new System.EventHandler(this.buttonLoadBank_Click);
            // 
            // buttonLoadAll
            // 
            this.buttonLoadAll.Location = new System.Drawing.Point(278, 284);
            this.buttonLoadAll.Name = "buttonLoadAll";
            this.buttonLoadAll.Size = new System.Drawing.Size(268, 23);
            this.buttonLoadAll.TabIndex = 2;
            this.buttonLoadAll.Text = "Загрузить всю таблицу";
            this.buttonLoadAll.UseVisualStyleBackColor = true;
            this.buttonLoadAll.Click += new System.EventHandler(this.buttonLoadAll_Click);
            // 
            // dataGridViewBanks
            // 
            this.dataGridViewBanks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBanks.Location = new System.Drawing.Point(12, 41);
            this.dataGridViewBanks.Name = "dataGridViewBanks";
            this.dataGridViewBanks.Size = new System.Drawing.Size(534, 237);
            this.dataGridViewBanks.TabIndex = 3;
            // 
            // comboBoxBanksName
            // 
            this.comboBoxBanksName.FormattingEnabled = true;
            this.comboBoxBanksName.Location = new System.Drawing.Point(13, 13);
            this.comboBoxBanksName.Name = "comboBoxBanksName";
            this.comboBoxBanksName.Size = new System.Drawing.Size(205, 21);
            this.comboBoxBanksName.TabIndex = 4;
            // 
            // buttonBuyDollar
            // 
            this.buttonBuyDollar.Location = new System.Drawing.Point(13, 313);
            this.buttonBuyDollar.Name = "buttonBuyDollar";
            this.buttonBuyDollar.Size = new System.Drawing.Size(258, 23);
            this.buttonBuyDollar.TabIndex = 5;
            this.buttonBuyDollar.Text = "Самый дешевый доллар для покупки";
            this.buttonBuyDollar.UseVisualStyleBackColor = true;
            this.buttonBuyDollar.Click += new System.EventHandler(this.buttonBuyDollar_Click);
            // 
            // buttonSellDollar
            // 
            this.buttonSellDollar.Location = new System.Drawing.Point(13, 343);
            this.buttonSellDollar.Name = "buttonSellDollar";
            this.buttonSellDollar.Size = new System.Drawing.Size(258, 23);
            this.buttonSellDollar.TabIndex = 6;
            this.buttonSellDollar.Text = "Самый дорогой доллар для продажи";
            this.buttonSellDollar.UseVisualStyleBackColor = true;
            this.buttonSellDollar.Click += new System.EventHandler(this.buttonSellDollar_Click);
            // 
            // buttonBuyEuro
            // 
            this.buttonBuyEuro.Location = new System.Drawing.Point(278, 313);
            this.buttonBuyEuro.Name = "buttonBuyEuro";
            this.buttonBuyEuro.Size = new System.Drawing.Size(268, 23);
            this.buttonBuyEuro.TabIndex = 7;
            this.buttonBuyEuro.Text = "Самый дешевый евро для покупки";
            this.buttonBuyEuro.UseVisualStyleBackColor = true;
            this.buttonBuyEuro.Click += new System.EventHandler(this.buttonBuyEuro_Click);
            // 
            // buttonSellEuro
            // 
            this.buttonSellEuro.Location = new System.Drawing.Point(278, 343);
            this.buttonSellEuro.Name = "buttonSellEuro";
            this.buttonSellEuro.Size = new System.Drawing.Size(268, 23);
            this.buttonSellEuro.TabIndex = 8;
            this.buttonSellEuro.Text = "Самый дорогой евро для продажи";
            this.buttonSellEuro.UseVisualStyleBackColor = true;
            this.buttonSellEuro.Click += new System.EventHandler(this.buttonSellEuro_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 380);
            this.Controls.Add(this.buttonSellEuro);
            this.Controls.Add(this.buttonBuyEuro);
            this.Controls.Add(this.buttonSellDollar);
            this.Controls.Add(this.buttonBuyDollar);
            this.Controls.Add(this.comboBoxBanksName);
            this.Controls.Add(this.dataGridViewBanks);
            this.Controls.Add(this.buttonLoadAll);
            this.Controls.Add(this.buttonLoadBank);
            this.Controls.Add(this.LoadBanksName);
            this.Name = "FormMain";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBanks)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button LoadBanksName;
        private System.Windows.Forms.Button buttonLoadBank;
        private System.Windows.Forms.Button buttonLoadAll;
        private System.Windows.Forms.DataGridView dataGridViewBanks;
        private System.Windows.Forms.ComboBox comboBoxBanksName;
        private System.Windows.Forms.Button buttonBuyDollar;
        private System.Windows.Forms.Button buttonSellDollar;
        private System.Windows.Forms.Button buttonBuyEuro;
        private System.Windows.Forms.Button buttonSellEuro;
    }
}

