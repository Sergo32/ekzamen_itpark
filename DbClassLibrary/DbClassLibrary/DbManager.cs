﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using MySql;
using DbClassLibrary.Table;

namespace DbClassLibrary
{

	public class DbManager
	{
		private static DbManager instance = null;

		public static DbManager GetInstance()
		{
			if (instance == null)
			{
				instance = new DbManager();
			}

			return instance;
		}

		public TableBank tableBank { get; private set; }


		public DbManager()
		{
			string connectionString = "Server=localhost;User=root;Password=1234;Database=parsing_banks";

			tableBank = new TableBank(connectionString);

		}
	}

}
