﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbClassLibrary.Entities
{
	public class Bank
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public double BuyDollarPrice { get; set; }

		public double SellDollarPrice { get; set; }

		public double BuyEuroPrice { get; set; }

		public double SellEuroPrice { get; set; }

		public string DT { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
