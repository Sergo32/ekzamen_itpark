﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using DbClassLibrary.Entities;

namespace DbClassLibrary.Table
{

	public class TableBank
	{
		private string connectionString;

		public TableBank(string connectionString)
		{
			this.connectionString = connectionString;
		}

		public List<Bank> FindBankByName(string nameBank)

		{
			List<Bank> banks = null;
			try
			{
				using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
				{
					mySqlConnection.Open();

					using (MySqlCommand mySqlCommand = new MySqlCommand())
					{
						mySqlCommand.Connection = mySqlConnection;
						mySqlCommand.CommandText = $"Select * From `banktable` where `name`='{nameBank}'";

						MySqlDataReader reader = mySqlCommand.ExecuteReader();

						banks = new List<Bank>();

						while (reader.Read())
						{
                            Bank bank = new Bank()
                            {
                                Id = reader.GetInt32("id"),
                                Name = reader.GetString("name"),
                                BuyDollarPrice = reader.GetDouble("buyDollarPrice"),
                                SellDollarPrice = reader.GetDouble("sellDollarPrice"),
                                BuyEuroPrice = reader.GetDouble("buyEuroPrice"),
                                SellEuroPrice = reader.GetDouble("sellEuroPrice"),
                                DT = reader.GetDateTime("DT").ToString()


							};


							banks.Add(bank);
						}
					}

					mySqlConnection.Close();
				}
			}
			catch (Exception e)
			{

				Console.WriteLine(e);
				throw;
			}

			return banks;
		}

		public List<Bank> GetAllBankName()

		{
			List<Bank> banks = null;
			try
			{
				using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
				{
					mySqlConnection.Open();

					using (MySqlCommand mySqlCommand = new MySqlCommand())
					{
						mySqlCommand.Connection = mySqlConnection;
						mySqlCommand.CommandText = $"Select `name` From `banktable`";

						MySqlDataReader reader = mySqlCommand.ExecuteReader();

						banks = new List<Bank>();

						while (reader.Read())
						{
							Bank bank = new Bank()
							{
								Name = reader.GetString("name")
							};


							banks.Add(bank);
						}
					}

					mySqlConnection.Close();
				}
			}
			catch (Exception e)
			{

				Console.WriteLine(e);
				throw;
			}

			return banks;
		}


		public List<Bank> FindDollarToBuy()

		{
			List<Bank> banks = null;
			try
			{
				using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
				{
					mySqlConnection.Open();

					using (MySqlCommand mySqlCommand = new MySqlCommand())
					{
						mySqlCommand.Connection = mySqlConnection;
						mySqlCommand.CommandText = $"Select `name`,`buyDollarPrice` From `banktable` where `buyDollarPrice`=(select Min(buyDollarPrice) from banktable)";

						MySqlDataReader reader = mySqlCommand.ExecuteReader();

						banks = new List<Bank>();

						reader.Read();
						{
							Bank bank = new Bank()
							{
						
								Name = reader.GetString("name"),
								BuyDollarPrice = reader.GetDouble("buyDollarPrice")
					


							};


							banks.Add(bank);
						}
					}

					mySqlConnection.Close();
				}
			}
			catch (Exception e)
			{

				Console.WriteLine(e);
				throw;
			}

			return banks;
		}


		public List<Bank> FindEuroToBuy()

		{
			List<Bank> banks = null;
			try
			{
				using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
				{
					mySqlConnection.Open();

					using (MySqlCommand mySqlCommand = new MySqlCommand())
					{
						mySqlCommand.Connection = mySqlConnection;
						mySqlCommand.CommandText = $"Select `name`,`buyEuroPrice` From `banktable` where `buyEuroPrice`=(select Min(buyEuroPrice) from banktable)";

						MySqlDataReader reader = mySqlCommand.ExecuteReader();

						banks = new List<Bank>();

						reader.Read();
						{
							Bank bank = new Bank()
							{

								Name = reader.GetString("name"),
                                BuyEuroPrice = reader.GetDouble("buyEuroPrice")



							};


							banks.Add(bank);
						}
					}

					mySqlConnection.Close();
				}
			}
			catch (Exception e)
			{

				Console.WriteLine(e);
				throw;
			}

			return banks;
		}

        public List<Bank> FindEuroToSell()

        {
            List<Bank> banks = null;
            try
            {
                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $"Select `name`,`sellEuroPrice` From `banktable` where `sellEuroPrice`=(select Max(sellEuroPrice) from banktable)";

                        MySqlDataReader reader = mySqlCommand.ExecuteReader();

                        banks = new List<Bank>();

                        reader.Read();
                        {
                            Bank bank = new Bank()
                            {

                                Name = reader.GetString("name"),
                                SellEuroPrice = reader.GetDouble("sellEuroPrice")



                            };


                            banks.Add(bank);
                        }
                    }

                    mySqlConnection.Close();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                throw;
            }

            return banks;
        }

        public List<Bank> FindDollarToSell()

        {
            List<Bank> banks = null;
            try
            {
                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $"Select `name`,`sellDollarPrice` From `banktable` where `sellDollarPrice`=(select Max(sellDollarPrice) from banktable)";

                        MySqlDataReader reader = mySqlCommand.ExecuteReader();

                        banks = new List<Bank>();

                        reader.Read();
                        {
                            Bank bank = new Bank()
                            {

                                Name = reader.GetString("name"),
                                SellDollarPrice = reader.GetDouble("sellDollarPrice")
                            };


                            banks.Add(bank);
                        }
                    }

                    mySqlConnection.Close();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                throw;
            }

            return banks;
        }

        public List<Bank> GetAllBank()

		{
			List<Bank> banks = null;
			try
			{
				using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
				{
					mySqlConnection.Open();

					using (MySqlCommand mySqlCommand = new MySqlCommand())
					{
						mySqlCommand.Connection = mySqlConnection;
						mySqlCommand.CommandText = $"Select * From `banktable`";

						MySqlDataReader reader = mySqlCommand.ExecuteReader();

						banks = new List<Bank>();

						while (reader.Read())
						{
							Bank bank = new Bank()
							{
								Id = reader.GetInt32("id"),
								Name = reader.GetString("name"),
								BuyDollarPrice = reader.GetDouble("buyDollarPrice"),
								SellDollarPrice = reader.GetDouble("sellDollarPrice"),
								BuyEuroPrice = reader.GetDouble("buyEuroPrice"),
								SellEuroPrice = reader.GetDouble("sellEuroPrice"),
								DT = reader.GetString("DT")


							};


							banks.Add(bank);
						}
					}

					mySqlConnection.Close();
				}
			}
			catch (Exception e)
			{

				Console.WriteLine(e);
				throw;
			}

			return banks;
		}



		public void ClearBank()
		{
			try
			{
				using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
				{
					mySqlConnection.Open();

					using (MySqlCommand mySqlCommand = new MySqlCommand())
					{
						mySqlCommand.Connection = mySqlConnection;
						mySqlCommand.CommandText = "Truncate Table `banktable`";
						mySqlCommand.ExecuteNonQuery();
					}

					mySqlConnection.Close();
				}
			}
			catch (Exception e)
			{

				Console.WriteLine(e);
				throw;
			}

		}



		public void InsertBank(Bank bank)
		{
			string time = DateTime.Now.ToShortDateString();
			try
			{
				using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
				{
					mySqlConnection.Open();

					using (MySqlCommand mySqlCommand = new MySqlCommand())
					{
						mySqlCommand.Connection = mySqlConnection;
						mySqlCommand.CommandText = $"INSERT INTO `banktable` (`name`,`buyDollarPrice`,`sellDollarPrice`,`buyEuroPrice`,`sellEuroPrice`,`DT` ) VALUES ('{bank.Name}',{bank.BuyDollarPrice.ToString(System.Globalization.CultureInfo.InvariantCulture)},{bank.SellDollarPrice.ToString(System.Globalization.CultureInfo.InvariantCulture)},{bank.BuyEuroPrice.ToString(System.Globalization.CultureInfo.InvariantCulture)},{bank.SellEuroPrice.ToString(System.Globalization.CultureInfo.InvariantCulture)},'{DateTime.Now:yyyy-MM-dd} {bank.DT}:00')";
						mySqlCommand.ExecuteNonQuery();
					}

					mySqlConnection.Close();
				}
			}
			catch (Exception e)
			{

				Console.WriteLine(e);
				throw;
			}
		}
	}
}


