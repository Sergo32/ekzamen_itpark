﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using DbClassLibrary.Entities;
using DbClassLibrary;

namespace WcfServiceItParkTest
{

    public class ServiceBank : IServiceBank
    {
        public List<Bank> GetAllBankNames()
        {
            return DbManager.GetInstance().tableBank.GetAllBankName();
        }

        public List<Bank> GetAllTable()
        {
            return DbManager.GetInstance().tableBank.GetAllBank();
        }

        public List<Bank> GetBankByName(string name)
        {
            return DbManager.GetInstance().tableBank.FindBankByName(name);
        }

        public List<Bank> GetDollarToBuy()
        {
            return DbManager.GetInstance().tableBank.FindDollarToBuy();
        }

        public List<Bank> GetDollarToSell()
        {
            return DbManager.GetInstance().tableBank.FindDollarToSell();
        }

        public List<Bank> GetEuroToBuy()
        {
            return DbManager.GetInstance().tableBank.FindEuroToBuy();
        }

        public List<Bank> GetEuroToSell()
        {
            return DbManager.GetInstance().tableBank.FindEuroToSell();
        }

        public bool Auth(Wrapper wrapper)
        {
            if (wrapper.Login == "abc" && wrapper.Password == "123")
            {
                return true;
            }
            return false;
        }
    }
}
