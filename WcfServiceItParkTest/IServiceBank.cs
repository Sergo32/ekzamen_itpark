﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using DbClassLibrary;
using DbClassLibrary.Entities;

namespace WcfServiceItParkTest
{
    [ServiceContract]
    public interface IServiceBank
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Auth", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool Auth(Wrapper wrapper);

        [OperationContract]
        [WebGet(UriTemplate = "/GetAllBankNames", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<Bank> GetAllBankNames();

        [OperationContract]
        [WebGet(UriTemplate = "/GetBankByName?name={name}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<Bank> GetBankByName(string name);

        [OperationContract]
        [WebGet(UriTemplate = "/GetAllTable", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<Bank> GetAllTable();

        [OperationContract]
        [WebGet(UriTemplate = "/GetDollarToBuy", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<Bank> GetDollarToBuy();

        [OperationContract]
        [WebGet(UriTemplate = "/GetDollarToSell", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<Bank> GetDollarToSell();

        [OperationContract]
        [WebGet(UriTemplate = "/GetEuroToBuy", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<Bank> GetEuroToBuy();

        [OperationContract]
        [WebGet(UriTemplate = "/GetEuroToSell", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<Bank> GetEuroToSell();
    }
}
